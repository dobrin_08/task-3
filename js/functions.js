;(function ($, window, document, undefined) {
  var $win = $(window);
  var $doc = $(document);

  $doc.ready(function () {
    // Remove custom added error class from label
    $('input[type=radio]').on('click', function () {
      $(this).closest('.form-col').find('.form-label').removeClass('error');
    });

    // Jqueryvalidation Init
    $("#form").validate({
      rules: {
        username: {
          required: true,
          minlength: 2
        },
        password: {
          required: true,
          minlength: 5
        },
        phone: {
          required: true,
        },
        email: {
          required: true,
          email: true
        },
        city: {
          required: true,
        },
        interestedIn: {
          required: true,
        },
        radios: {
          required: true,
        },
      },
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          element.closest('.form-col').find('.form-label').addClass('error');
        }
      }
    });
  });
})(jQuery, window, document);
